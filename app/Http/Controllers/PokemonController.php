<?php

namespace App\Http\Controllers;

use App\Pokemon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class PokemonController extends Controller
{
    public function index()
    {
        $localcreatures = $this->getLocalPokemon();

        $remotecreatures = $this->getRemotePokemon();

        if (count($localcreatures) != $remotecreatures->count) {
            $remotecreatures->results;

            foreach ($remotecreatures->results as $key => $result) {
                $url = parse_url($result->url);

                $url = explode('/', $url['path']);
                $id = $url[4];

                $creatures[] = [
                    'id' => $id,
                    'name' => $result->name
                ];
            }

            $type = 'remote';
        } else {
            $creatures = $localcreatures;
            $type = 'local';
        }

        $title = 'PokeDex';

        return view('main', [
                'title' => $title,
                'creatures' => $creatures,
                'type' => $type
            ]
        );
    }

    protected function getLocalPokemon(string $store = 'pokemon')
    {
        $creatures = Cache::get($store);

        return $creatures;
    }

    protected function getRemotePokemon(string $name = '')
    {
        $url = 'http://pokeapi.co/api/v2/pokemon/' . $name;

        $list = @file_get_contents($url);

        if (!$list) {
            abort(404, 'The remote resource is offline');
        }

        $json = json_decode($list);

        $creatures = $json;

        if ($name == '') {
            Cache::forever('pokemon', $creatures);
        }
        return $creatures;
    }

    public function show($name)
    {
        $creature = $this->getLocalPokemon($name);

        if (empty($creature)) {
            $creature = $this->getRemotePokemon($name);
            $species = $this->getPokemonSpecies($creature->id);

            $cache = new \stdClass();
            $cache->id = $creature->id;
            $cache->name = $creature->name;
            $cache->species = $species;
            $cache->height = $creature->height;
            $cache->weight = $creature->weight;
            $cache->abilities = $this->getPokemonAbilities($creature->abilities);

            Cache::put($name, $cache, 60);

            $creature = $cache;
        }

        $title = 'PokeDex - ' . ucfirst($creature->name) . ' Details';

        return view('details',
            [
                'creature' => $creature,
                'title' => $title,
            ]
        );
    }

    protected function getPokemonSpecies(int $id): string
    {
        $url = 'http://pokeapi.co/api/v2/pokemon-species/' . $id;

        $list = file_get_contents($url);

        $genera = json_decode($list)->genera;

        foreach ($genera as $genus) {
            if ($genus->language->name == 'en') {
                $species = $genus->genus;
            }
        }

        return $species;
    }

    protected function getPokemonAbilities($abilities)
    {
        foreach ($abilities as $ability) {
            $cleanAbilities[] = $ability->ability->name;
        }

        return $cleanAbilities;
    }

    public function search()
    {
        $search=strtolower(\request('search'));

        $creatures = $this->getLocalPokemon()->results;

        foreach($creatures as $creature){
            $test=stristr(strtolower($creature->name),$search);
            
            if($test !=false) {
                $url = parse_url($creature->url);

                $url = explode('/', $url['path']);
                $id = $url[4];

                $results[] = [
                    'id' => $id,
                    'name' => $creature->name
                ];
            }
        }

        $title = 'PokeDex - Search Results: '.$search;

        return view('main', [
                'title' => $title,
                'creatures' => $results,
                'type' => 'local'
            ]
        );
    }
}