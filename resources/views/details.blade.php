@include('partials.header')

<body>

<div class="container">
    @include('partials.navbar')

    <div class="jumbotron p-3 p-md-5 text-white bg-dark px-0">
        <div class="row">
        <div class="col-md-6 px-0">
            <h1 class="display-4 font-italic">{{ucfirst($creature->name)}}</h1>
            <div class="p-3">
                <h4 class="font-italic">Statistics</h4>
                <ul class="list-unstyled mb-0">
                    <li><strong>Species:</strong> {{$creature->species}}</li>
                    <li><strong>Height:</strong> {{$creature->height/10}} metres</li>
                    <li><strong>Weight:</strong> {{$creature->weight/10}} kg</li>
                </ul>
            </div>
            <div class="p-3">
                <h4 class="font-italic">Abilities</h4>
                <ul class="list-unstyled mb-0">
                    @foreach($creature->abilities as $ability)
                        <li>{{ucfirst($ability)}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-6 px-0">
            <img src="{{asset('/sprites/pokemon/other-sprites/official-artwork/'.$creature->id.'.png')}}">
        </div>
        </div>
    </div>
</div>
@include('partials.footer')