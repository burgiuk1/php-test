@include('partials.header')

<body>
@include('partials.navbar')
<main role="main">

    @include('partials.searchbar')

    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row">
                @include('partials.card')
            </div>
        </div>
    </div>
</main>

@include('partials.footer')