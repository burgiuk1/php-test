<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">{{$title}}</h1>
        <p class="lead text-muted">Enter the name of a Pokemon to search or browse the list</p>
        <form action="/" method="post">
            @csrf
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="button-addon2" id="search" name="search">
            <div class="input-group-append">
                <button class="btn btn-outline-primary" type="submit" id="button-addon2">Search</button>
            </div>
        </div>
    </form>
    </div>
</section>