@foreach($creatures as $creature)

<div class="col-md-3">
    <div class="card mb-3 shadow-sm">
        @if(file_exists(public_path().'/sprites/pokemon/'.$creature['id'].'.png'))
        <img class="card-img-top" src="{{asset('/sprites/pokemon/'.$creature['id'].'.png')}}" alt="Card image cap">
        @else
            <img class="card-img-top" src="{{asset('/sprites/pokemon/0.png')}}" alt="Card image cap">
        @endif
        <div class="card-body">
            <h3 class="card-text">{{ ucfirst($creature['name']) }}</h3>
            <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <a href="/pokemon/{{$creature['name']}}" role="button" class="btn btn-sm btn-outline-secondary">View</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endforeach